---
title: "Understanding commuting patterns using transit smart card data"
author: "Xiaolei Ma"
id: 20220411152706
saisie: 2022-04-11
date: 2017
DOI/ISBN/URL: https://doi.org/10.1016/j.jtrangeo.2016.12.001
ID_Zotero: ma2017UnderstandingCommutingPatterns
keywords:
  - bibliography
  - mobilité
  - migration pendulaire
  - carte de chaleur
  - données massives
---

# Résumé en 3 phrases
L'étude présente une méthode qui permet de catégoriser les comportements de mobilités à partir de données massives issues des carte d'abonnement du réseau de transport de Beijing (Chine).
Les auteurs présentent une méthode automatique à l'aide de plusieurs algorithmes qui donnent de bons résultats.
Les résultats de l'étude permettent également de conforter des résultats d'autres études.

# Idées / réflexions
- vérification de la méthode à l'aide d'un questionnaire dans lequel les usagers pouvaient donner l'identifiant de leur carte d'abonnement.
- Distance moyenne des navettes calculées selon quelle métrique ? distance à vol d'oiseau ? distance selon le réseau ?
- au départ ca qui m'intéressait dans cet article, c'était les horaires des navetteurs pour un article sur les sons ambiants à certaines heures en France métropolitaine. Est-ce qu'il n'y a pas une source équivalente plus proche (France ou Europe) ou plus basée sur les horaires de navettage ? Un (très succincte) revue des articles sur le sujet parle soit de la distance idéale, soit du temps idéal mais pas des horaires idéaux.


# Qu'est ce que cet article m'a apporté
- meilleure connaissance des travaux autour des cartes d'abonnement et ce que l'on peut faire avec
- exemple de traitement de données massives
- 
# Top 3 des citations

> La majorité des navetteurs quittent leur domicile durant les heures de pointe du matin (7h00-9h00) et rentrent durant les heures de pointe du soir (17h00-19h00). _The majority of transit commuters depart from their homes around morning peak hours (7:00 AM–9:00 AM) and return during evening peak hours (5:00 PM–7:00 PM)_ (Ma et al. 2017)

> Un trajet est composé d'une séquence d'activités pour un objectif particulier. _A trip is composed of a sequence of activities for a particular purpose_ (Primerano et al., 2008).

> Un navetteur est défini commun un usager régulier qui effectue des trajets périodiquement récurrents entre sa résidence et un autre lieu non résidentiel (fait référence principalement à un trajet répété entre le domicile et le travail ou l'école). _A commuter is defined as a regular transit rider who performs periodically recurring travels between home and other nonresidential locations (primarily refers to the repeated trip between home and work/school)_ (Kung et al., 2014).

# Résumé et notes
## Vocabulaire
- _smart card_: carte d'abonnement de transport
- _commuter_: navetteur
- _alighting time_ : heure de descente

## Introduction
L'étude se penche sur les motifs (_patterns_) spatio-temporels de navetteurs à Beiijing sur une période d'un mois.

Le déséquilibre spatial entre zones d'habitation et zones d'emploi force les gens à pratiquer des navettes coûteuses en terme de temps et de carburant (Van Acker 2011). Ces nombreuses navettes influent considérablement les conditions de trafic (Zhou, 2014)

L'offre de transport est une des meilleur stratégie pour réduire la dépendance à l'automobile et la pollution (Ma, 2014).

L'étude des comportements individuels est plus précise mais plus complexe. L'obtention de données de la part de volontaires est ardue. L'utilisation des données des cartes d'abonnement permet de gagner en précision tout en éliminant les variations.

De nombreuses études ont été faites autour des cartes d'abonnement avec des regroupements sur des critères socio-économiques et les motifs de mobilités.

Questions auxquelles ce papier essaie de répondre:
- Qui sont les navetteurs et quels sont leurs usages ? Comment les distinguer ?
- Peut-on déterminer la localisation et l'heure des départs et arrivées ?

## Méthodologie
Le système de transport de Beijing est important: 1000 routes de bus, 18 lignes de métro, 28 000 arrêt de bus et de métro.
Grâce à un effort fait de développement des infrastructures et les aides gouvernementales, le transport public à Beijing représenté en 2014 60% des transports motorisés.

L'étude porte sur 1 mois (juin 2015) soit  364 millions de transactions  18 million d'usagers ont pu être identifiés.
Dans 60% ces cas, les chercheurs disposent de l'heure de prise en charge et de l'heure de descente (40% manquant majoritairement en ville). Les trajets manquants ont été modélisés à partir des temps moyens de trajets connus.

La génération des trajets a été fait de manière journalière. Un trajet peut être associé à plusieurs transactions. 

Des seuils ont été définis selon les trajets (bus → métro, bus → bus, métro → bus) et du temps de trajet piéton estimé. Si le temps entre 2 transactions dépassent ces seuils, 2 trajets sont distingués.

### Regroupement des arrêts proches
Un usagers peut utiliser différents arrêts proches de sa destination selon la route qu'il aura choisi. Les auteurs de l'article ont utilisés un algorithme DBSCAN modifié pour regrouper des arrêts spatialement adjacents. Un algorithme DBSCAN normal aurait regroupé les arrêts qui appartiennent à plusieurs groupes dans un groupe distinct. L'algorithme modifié réaffecte les arrêts de ces groupes anormaux et sépare les grands clusters en plusieurs clusters plus petits. 28871 arrêts ont été regroupés en 6544 groupes d'arrêts.


### Calculs des trajets
Seuls les premiers et derniers trajets de la journée sont pris en compte. Il s'agit de ceux qui ont le plus de chances de partir et de revenir au domicile de l'usager.

Pour chaque usager, les auteurs déterminent les groupes d'arrêt les plus fréquents et les plus routes les plus fréquentes.

### Identification des navetteurs/ non-navetteurs

La méthode TOPSIS a été utilisé pour définir pour chaque usager un score de navettage en fonction d'une évaluation multi-critère du comportement de navettage.

Pour identifier les navetteurs, l'équipe a utilisé l'algorithme ISODATA à partir des régularités spatio-temporelles des usagers plutôt que de calculer un score de régularité avec un seuil arbitraire. L'algorithme a réalisé les regroupements et déterminer le seuil du score de régularité entre navetteurs et non-navetteurs.

La quantité de donnés est trop importante pour être exploitée entièrement avec ISODATA, les auteurs ont fait le choix de travailler sur 5 tirages au sort de 500 000 usagers. Chaque tirage produit 3 groupes : navetteurs absolus (_absolute commuters_), navetteurs réguliers (_average commuters_), et non-navetteurs (_noncommuters_).

### Validation de la méthode
Une enquête complémentaire a été réalisée pour valider la méthode, qui en plus des informations concernant la localisation de leur domicile et de leur travail demandait aussi **leur ID de carte d'abonnement**.

En croisant les deux bases, les chercheurs obtiennent une précision de 94,1% sur la détection du comportement de navettage.

### Points chauds
Des cartographies sous forme de [[carte de chaleur)]] ont été réalisé pour visualiser les zones les plus sollicitées.

Une carte des lieux d'habitation et une carte des lieux d'emploi ont été réalisées.
La carte des leiux de travail mets en valeur des zones d'emplois connues : centres financiers, universités, centres de recherche, agences gouvernementales. De la même manière, la carte des lieux d’habitation montre des zones densément peuplées connues.

La comparaison de ces cartes donne à voir un déséquilibre fort habitation/travail.
Ainsi, 80% des usagers vivent au-delà de la 3ème route circulaire alors que 34% des lieux d'emploi sont situés en son sein. Les auteurs font l'hypothèse qu'un grand nombre de gens qui travaillent dans ces zones ne peuvent se permettre les loyers élevés pratiqués dans les zones d'emploi.

## Discussion

La majorité des navetteurs quittent leur domicile durant les heures de pointe du matin (7h00-9h00) et rentrent durant les heures de pointe du soir (17h00-19h00).
Un navetteur régulier dans l'étude fait en moyenne des trajets réguliers 21jours  par mois, les non-navetteurs en font 10 jours par mois  à des horaires aléatoires.

La distance moyenne de navette de l'étude est de 10.99 km, la distance moyenne pour les non navetteurs est de 9.15 km mais avec une médiane inférieur à 5 km.

## Conclusion
La méthode automatique utilisée donne de très bons résultats sur l'identification des comportements de mobilités à partir des transactions de cartes d'abonnement.
L'étude confirme également le déséquilibre entre lieux d'habitation et lieux de travail.

# Référence bibliographique
Ma, Xiaolei, Congcong Liu, Huimin Wen, Yunpeng Wang, et Yao-Jan Wu. 2017. « Understanding Commuting Patterns Using Transit Smart Card Data ». _Journal of Transport Geography_ 58 (janvier): 135‑45. https://doi.org/10.1016/j.jtrangeo.2016.12.001.

##  extrait de la bibliographie de l'article
Charron, M., 2007. From excess commuting to commuting possibilities: more extension to the concept of excess commuting. Environ. Plan. A 39, 1238–1254.

Kung, K., Greco, K., Sobolevsky, S., Ratti, C., 2014. Exploring universal patterns in human home-work commuting from mobile phone data. PLoS One 9 (6), e96180.

Ma, X., Wang, Y., 2014. Development of a data-driven platform for transit performance measures using smart card data and GPS data. J. Transp. Eng. 140, 04014063.

Van Acker, V., Witlox, F., 2011. Commuting trips within tours: how is commuting related to land use? Transportation 38, 465–486.

Primerano, F., Taylor, M.A.P., Pitaksringkarn, L., Tisato, P., 2008. Defining and understanding trip chaining behavior. Transportation 35, 55–72.

Zhou, J., Murphy, E., Long, Y., 2014. Commuting efficiency in the Beijing metropolitan area: an exploration combining smart card and travel survey data. J. Transp. Geogr. 41, 175–183.
---
title: "Effect of time of the day on bird activity"
author: "Chandler S. Robbins"
id: 20220411150327
saisie: 2022-04-11
date: 1981
URL: https://sora.unm.edu/node/139113
ID_Zotero: robbins1981EffectTimeDay
keywords:
  - bibliography
  - ornithologie
  - acoustique environnementale
---
@robbins1981EffectTimeDay

# Résumé en 3 phrases
Robbins agrège des relevés de présence (animal vu ou entendu) d'oiseaux en Amérique du Nord durant 5 ans (1970 - 1974).
Il étudie la fréquence de l'activité relevée à plusieurs moments de la journée à différentes périodes de l'année.


# Idées / réflexions
Dans l'étude des données Noisecapture de 2017 à 2020, le pic d'activité une heure autour et après le levé du soleil est visible.
Mais le point bas est plus tard vers la fin d'après-midi. L'activité plus fortement matinale en hiver n'a pas été étudiée.

# Qu'est ce que cet article m'a apporté

# Top 3 des citations

# Résumé et notes
Robbins note un pic d'activité durant l'heure centrée sur le levé du soleil et l'heure suivante avec un point bas à midi.
En hiver l'activité est surtout présente tôt le matin.